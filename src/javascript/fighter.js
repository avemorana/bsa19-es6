class Fighter{
    name;
    health;
    attack;
    defense;

    min = 1;
    max = 2;

    constructor(fighter){
        this.name = fighter.name;
        this.health = fighter.health;
        this.attack = fighter.attack;
        this.defense = fighter.defense;
    }

    getHitPower(){
        return this.attack * (Math.random() * (this.max - this.min) + this.min);
    }

    getBlockPower(){
        return this.defense * (Math.random() * (this.max - this.min) + this.min);
    }

    get name(){
        return this.name;
    }

    get health(){
        return this.health;
    }

    get attack(){
        return this.attack;
    }

    get defense(){
        return this.defense;
    }

    set name(value){
        this.name = value;
    }

    set health(value){
        this.health = value;
    }

    set attack(value){
        this.attack = value;
    }

    set defense(value){
        this.defense = value;
    }
}

export default Fighter;