import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import PanelView from './panelView';

class App {
	constructor() {
		this.startApp();
	}

	static rootElement = document.getElementById('root');
	static loadingElement = document.getElementById('loading-overlay');

	async startApp() {
		try {
			App.loadingElement.style.visibility = 'visible';
			
			const fighters = await fighterService.getFighters();
			const fightersView = new FightersView(fighters);
			const fightersElement = fightersView.element;

			const panelView = new PanelView(fightersView.handleStartClick);
			const panelElement = panelView.createPanel();

			App.rootElement.appendChild(panelElement);
			App.rootElement.appendChild(fightersElement);
		} catch (error) {
			console.warn(error);
			App.rootElement.innerText = 'Failed to load data';
		} finally {
			App.loadingElement.style.visibility = 'hidden';
		}
	}
}

export default App;