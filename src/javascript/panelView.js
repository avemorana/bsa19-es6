import View from './view';

class PanelView extends View{

    fighter1ELement;
    fighter2ELement;

    static fighter1;
    static fighter2;

    constructor(handleClick){
        super();
        this.handleClick = handleClick;
    }

    createPanel(){
        this.element = this.createElement({ tagName: 'div', className: 'panel' });
        const textElement = this.createElement({tagName: 'span', className: 'message'});
        textElement.innerText = 'For choosing fighter for fight (2 fighters) click on him.\n';
        this.fighter1ELement = this.createElement({tagName: 'span', className: 'fighter-name', attributes: {id: 'fighter1-name'}});
        this.fighter2ELement = this.createElement({tagName: 'span', className: 'fighter-name', attributes: {id: 'fighter2-name'}});
        this.element.append(textElement);
        this.element.append(this.fighter1ELement);
        this.element.append(this.fighter2ELement);
        this.createStartFightButton(this.handleClick);

        return this.element;
    }

    createStartFightButton(handleClick){
		const startFightButtonElement = this.createElement({ tagName: 'button', className: 'start-button'});
		startFightButtonElement.innerText = 'Start';
		startFightButtonElement.addEventListener('click', event => handleClick(event, PanelView.fighter1, PanelView.fighter2), false);
		this.element.append(startFightButtonElement);
	}
}

export default PanelView;