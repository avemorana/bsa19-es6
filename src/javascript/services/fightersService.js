import { callApi } from '../helpers/apiHelper';

class FighterService {
	getFighters() {
	  try {
			const endpoint = 'fighters.json';
			return this.getInfoFromApi(endpoint);
	  } catch (error) {
			throw error;
	  }
	}

	getFighterDetails(_id){
		try {
			const endpoint = 'details/fighter/' + _id + '.json';
			return this.getInfoFromApi(endpoint);
		} catch (error) {
			throw error;
		}
	}

	async getInfoFromApi(endpoint){
		const apiResult = await callApi(endpoint, 'GET');
		return JSON.parse(atob(apiResult.content));
	}

	fight(fighter1, fighter2){
		let win;
		let message = 'START FIGHT!\n' + fighter1.name + ' VS ' + fighter2.name + '\n';
		let i = 0;
			while(i <= 100){
				if (fighter1.health <= 0){
					message += 'fighter1 dead\n';
					win = fighter2;
					break;
				}
				if (fighter2.health <= 0){
					message += 'fighter2 dead\n';
					win = fighter1;
					break;
				}
				
				if (i & 1){
					// fighter2 attacks
					fighter1.health = fighter2.getHitPower() - fighter1.getBlockPower();
					message += 'fighter2 attacks => fighter1.health=' + fighter1.health + '\n';
				} else {
					// fighter1 attacks
					fighter2.health = fighter1.getHitPower() - fighter2.getBlockPower();
					message += 'fighter1 attacks => fighter2.health=' + fighter2.health + '\n';
				}
				i++;
			}
		message += 'WIN - ' + win.name + ' in ' + i + ' moves';
		this.logToScreen(message);

	}

	logToScreen(message){
		const textPanel = document.getElementById("text-log");
		textPanel.innerText = message;

		let fighter1Element = document.getElementById('fighter1-name');
		fighter1Element.innerText = 'Fighter 1: \n';
		let fighter2Element = document.getElementById('fighter2-name');
		fighter2Element.innerText = 'Fighter 2: \n';

	}
}
  
export const fighterService = new FighterService();