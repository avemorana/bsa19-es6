import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import Fighter from './fighter';
import PanelView from './panelView';


class FightersView extends View {
	fighter1;
	fighter2;
	flag = 1;

	constructor(fighters) {
		super();
		
		
		this.handleClick = this.handleFighterClick.bind(this);
		this.createFighters(fighters);
	}

	fightersDetailsMap = new Map();

	createFighters(fighters) {
		const fighterElements = fighters.map(fighter => {
		    const fighterView = new FighterView(fighter, this.handleClick);
		    return fighterView.element;
		});

		this.element = this.createElement({ tagName: 'div', className: 'fighters' });
		this.element.append(...fighterElements);
	}

	async handleFighterClick(event, fighter) {
		let fighterDetails = this.fightersDetailsMap.get(fighter._id);
		if (!fighterDetails){
			console.log('search...');
			fighterDetails = await fighterService.getFighterDetails(fighter._id);
			this.fightersDetailsMap.set(fighter._id, fighterDetails);
		}
		console.log(fighterDetails);
		let tmpFighter = new Fighter(fighterDetails);

		let confirmFighter = confirm("Fighter " + tmpFighter.name + "\n"
				+ "health: " + tmpFighter.health + "\n"
				+ "attack: " + tmpFighter.attack + "\n"
				+ "defence: " + tmpFighter.defense + ".\n"
				+ "Add to fight?");

		
		if (confirmFighter){
			if (this.flag & 1){
				this.fighter1 = new Fighter(tmpFighter);
				let fighter1Element = document.getElementById('fighter1-name');
				fighter1Element.innerText = 'Fighter 1: ' + this.fighter1.name + '\n';
				PanelView.fighter1 = this.fighter1;
			} else {
				this.fighter2 = new Fighter(tmpFighter);
				let fighter2Element = document.getElementById('fighter2-name');
				fighter2Element.innerText = 'Fighter 2: ' + this.fighter2.name + '\n';
				PanelView.fighter2 = this.fighter2;
			}
			this.flag++;
		}
		console.log('clicked')
		// get from map or load info and add to fightersMap
		// show modal with fighter info
		// allow to edit health and power in this modal
	}


	handleStartClick(event, fighter1, fighter2)
	{
		fighterService.fight(fighter1, fighter2);
	}
}

export default FightersView;